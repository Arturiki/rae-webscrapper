# -*- coding: utf-8 -*-
"""
Created on Sun Feb 16 20:36:00 2020

Web scraper for RAE.
Version 0.3

@author: Arturiki
"""
import time

from browser import get_link, get_result
from arg_parser import get_word_and_mode, define_parser


def main():

    start = time.time()

    parser = define_parser()

    [word, mode] = get_word_and_mode(parser)

    if word is not '' or mode is not '':
        link = get_link(word, mode)
        get_result(link, mode)

    print('Tiempo transcurrido: ' + str(round(time.time() - start, 2)) + ' segundos')


if __name__ == '__main__':
    main()
