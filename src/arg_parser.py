# -*- coding: utf-8 -*-
"""
Created on Sat Feb 22 18:00 2020

Web scraper for RAE.
Version 0.3

@author: Arturiki
"""
import argparse


def define_parser():

    parser = argparse.ArgumentParser(description='Buscar palabras en el '
                                                 'diccionario de la Real '
                                                 'Académica Española')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-a', '--anagrama', type=str,
                       help='Buscar anagramas de la palabra')
    group.add_argument('-am', '--americanismo', type=str,
                       help='Buscar americanismos de la palabra')
    group.add_argument('-d', '--duda', type=str,
                       help='Buscar palabra en el panhispánico de dudas')
    group.add_argument('-e', '--expresion', type=str,
                       help='Buscar expresiones de la palabra')
    group.add_argument('-j', '--juridico', type=str,
                       help='Buscar términología jurídica de la palabra')
    group.add_argument('-s', '--significado',
                       help='Buscar significado de la palabra')

    return parser


def get_word_and_mode(the_parser):

    args = the_parser.parse_args()

    the_word = ''
    the_mode = ''

    if args.anagrama is not None:
        print('Anagramas de ' + args.anagrama)
        the_word = args.anagrama
        the_mode = 'anagram'
    elif args.americanismo is not None:
        print('Americanismos de ' + args.americanismo)
        the_word = args.americanismo
        the_mode = 'americanism'
    elif args.duda is not None:
        print('Dudas sobre ' + args.duda)
        the_word = args.duda
        the_mode = 'doubt'
    elif args.expresion is not None:
        print('Expresiones de ' + args.expresion)
        the_word = args.expresion
        the_mode = 'idiom'
    elif args.juridico is not None:
        print('Término jurídico de ' + args.juridico)
        the_word = args.juridico
        the_mode = 'judicial'
    elif args.significado is not None:
        print('Significado de ' + args.significado)
        the_word = args.significado
        the_mode = 'meaning'
    else:
        print('Por favor, utiliza uno de los comandos indicados por --help.')

    return [the_word, the_mode]


