# -*- coding: utf-8 -*-
"""
Created on Sun Feb 23 21:21 2020

Web scraper for RAE.
Version 0.3

@author: Arturiki
"""

import browser


def get_meaning(the_browser, the_mode):

    if browser.is_in_dictionary(the_browser):
        browser.get_articles_and_children(the_browser, the_mode)
    else:
        browser.print_items_or_all(the_browser)


def get_idiom(the_browser, the_mode):

    if browser.is_in_dictionary(the_browser):
        browser.get_articles_and_children(the_browser, the_mode)
    else:
        print('No en el diccionario')
        browser.print_items_or_all(the_browser)


def get_doubt(the_browser, the_mode):

    if browser.is_in_dictionary(the_browser):
        browser.get_articles_and_children(the_browser, the_mode)


def get_judicial(the_browser, the_mode):

    if browser.is_in_dictionary(the_browser):
        browser.get_articles_and_children(the_browser, the_mode)


def get_anagram(the_browser, the_mode):

    browser.print_items_or_all(the_browser)


def get_americanism(the_browser, the_mode):

    if browser.is_in_dictionary(the_browser):
        browser.get_articles_and_children(the_browser, the_mode)


mode2method = {
    'anagram': get_anagram,
    'americanism': get_americanism,
    'doubt': get_doubt,
    'judicial': get_judicial,
    'idiom': get_idiom,
    'meaning': get_meaning,
    }
