# Changelog

All notable changes to this project will be documented in this file.

## [v0.3] - 2020-03-01

### Added
- Retrieve results for idioms
- Retrieve results for anagrams
- Handle NoSuchElementException

### Changed
- Clean up dependencies and simplify methods
- Display custom no-result message instead of generic message
- Improve speed time for results

### Removed
- Unneeded methods
- Uneeded logs
- Uneeded complexity


## [v0.2] - 2020-02-23

### Added
- Argument parser
- Retrieve meaning feature
- Set up all other feature commands
- Headers and new classes
- Main method

### Changed
- Refactor methods
- Small "v" in changelog for the versions


## [v0.1] - 2020-02-16

### Added

- First code approach.
- PEP8 formatting.
- CHANGELOG.
- LICENCE.
- CONTRIBUTING.
- Usecase and merge request templates.
- Logo image.
